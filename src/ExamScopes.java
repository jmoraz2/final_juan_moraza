import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


abstract class Cell{
    //size:
    // 100: whole cell
    // 1 : 1/10 of cell
    int nuclues = 10;
    int nucleolus = 2;
    int cytoplasm = 40;
    int totalSize = cytoplasm + nucleolus+nuclues;

}

/* NATIVE IMPLEMENTATION
 * my systeme was giving me this error so decided to skip it but
 * I follow what it was suggested on the book
 * error message:   /Library/Java/JavaVirtualMachines/openjdk-13.0.1.jdk/Contents/Home/bin/java "-javaagent:/Applications/IntelliJ IDEA.app/Contents/lib/idea_rt.jar=55105:/Applications/IntelliJ
 * did not want to risk it since not compile could cost my grade...
 *
 *  */

//class Native {
//        static
//        {
//        System.loadLibrary( "asdasd");
//        }
//        public native void m();
//}
// A simple interface
interface SimpleInterface
{
    //  final integer
    final String genralMessage = "interface being used... and it stop because definite assignment press ENTER to continue :)\n\n" +
            "*******DISCLAIMER********\n\tAll data and conversions that you would see are randombly calculated. Of course I do not know " +
            "how much water an animal or plant cell needs nor I know how much Newtons each muscles needs to function.\n" +
            "Press ENTER to continue...";
    final int a = 1;

    // public and abstract
    //for message in main
    void displayGeneralMessage();
}
interface TotalWater
{
    // An abstract function
    int abstractFun(int x);
    // A non-abstract (or default) function
    default void normalFun()
    {
        System.out.println("Hello");
    }
}

public class ExamScopes implements SimpleInterface{
    //identifier for max value of
    // options permited in the input value
    private static Integer MAX_VALUE = 10;

    @Override
    public void displayGeneralMessage() {
        System.out.println(genralMessage);
        //generall assignment to print somevalues while print last part of message
        {
            int k;
            try {
                if (a > 0 && (k = System.in.read()) >= 0)
                    System.out.println(k);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
     }

    //************** Organisims
    //Plant organisim
    static class Plant extends Organisim {
        int waterNeeded = 0;
        int organs = 0;
        RootSystem plantsRootSystem = new RootSystem();
        void printInfo(){
            System.out.println("This is a "+this.organisimType+" organisim\n" +
                    "This organisim needs " +this.waterNeeded+" ml/hr of water\n" +
                    "This is number of Organs "+this.plantsRootSystem.num_Organs);
        }

        Plant( String type  ) { //formal parameter for organism type
            this.organisimType = type;
            this.waterNeeded = this.plantsRootSystem.waterNeeded();
            this.organs = this.plantsRootSystem.numOfOrgans;
            this.printInfo();
        }
    }

    // Human organisim
    static class Human extends Organisim {
        public  Limbs limbs;
        int waterNeeded = 0;
        int organs = 0;
        ArrayList <String> organsystems = new ArrayList<>();
        MuscularSystem humanMuscularSystem = new MuscularSystem();
        void printInfo(){
            System.out.println("This is a "+this.organisimType+" organisim\n" +
                    "This organisim needs " +this.waterNeeded+" ml/hr of water\n" +
                    "This is number of Organs "+this.humanMuscularSystem.num_Organs);
        }

        String name;
        Human(String name, String type, int limbs) { //formal parameter for organisim type
            this.organisimType = type;
            this.name = name;
            this.limbs= new Limbs(limbs);
        }
        //parameterized constructor
        Human(String name, String type ) { //formal parameter for organisim type
            this.organisimType = type;
            this.name = "unknown name";
            this.waterNeeded = this.humanMuscularSystem.waterNeeded();
            this.organs = this.humanMuscularSystem.num_Organs;
            this.printInfo();
            organsystems.add("skeletal");
            organsystems.add("muscular");
            organsystems.add("immune");
            organsystems.add("reproductive");
            organsystems.add("integumentary");
        }

        Human(String name, String type, boolean info ) { //formal parameter for organisim type
            this.organisimType = type;
            this.name = "unknown";
            this.waterNeeded = this.humanMuscularSystem.waterNeeded();
            this.organs = this.humanMuscularSystem.num_Organs;
            if(info)
                this.printInfo();
            organsystems.add("skeletal");
            organsystems.add("muscular");
            organsystems.add("immune");
            organsystems.add("reproductive");
            organsystems.add("integumentary");
        }
        //multi levels of nesting
        class Limbs{
            public int limbs = 0;
            Limbs(int num){
                this.limbs = num;
            }
            public int returnLimbs(){
                return this.limbs;
            }
        }
    }
    //Polymorphism
    //Animal and Plant extends to Organisim class
    // simple body implemnted too
    static class Organisim {
        int numberOfOrganSystems = 0;
        public String organisimType = "";
        public int howManyOrganSystems(){
            return  this.numberOfOrganSystems;
        }
    }
    // ********* Organ Systems
    static class OrganSystem{
        int num_Organs = 0;
    }
    // Plamn
    static class MuscularSystem extends OrganSystem{
        MuscularSystem(){
            this.num_Organs = 232;
        }
        Muscles muscles = new Muscles();

        public void printMuscleNames(){
            for( String muscle : muscles.muscle_names){
                System.out.println(muscle);
            }
        }
        public int waterNeeded(){

            //lambda expresion to
            // multiply water by num of organs to
            //get total of water needed
            TotalWater fobj = (int x)-> (3*x);
            fobj.abstractFun(this.num_Organs );

            return   fobj.abstractFun(this.num_Organs );
        }
    }
    static class RootSystem extends OrganSystem{
        RootSystem(){
            this.num_Organs = 23;
        }
        int numOfOrgans = new Roots().numOrgans();
        public int waterNeeded(){
            //lambda expresion to
            // multiply water by num of organs to
            //get total of water needed
            TotalWater fobj = (int x)-> (3*x);
            fobj.abstractFun(this.num_Organs );

            return   fobj.abstractFun(this.num_Organs );
        }
    }
    //********** Organs
    //null object is implementd to later on
    //implement character identifier
    static class Organ{
        //null literal is used to tell that
        // as starting value oragan does not have a typeOrgan
        // since that is decided once we pick
        // between plant and animal
        public Character typeOrgan = null;
        public int numOfOrgans = 0;
    }

    // Organ: Plant
    static class Roots extends Organ{
        ArrayList<String> root_names= new ArrayList<String>();
        Roots(){
            root_names.add("Branch Roots");
            root_names.add("Taproot");
        }
        public int numOrgans(){
            this.numOfOrgans = 2;
            return this.numOfOrgans ;
        }
        //overiding null value to get P indentifier for Plant
    }
    // Organ: Animal
    static class Muscles extends Organ{
        int totalSize = 0;
        ArrayList<String> muscle_names= new ArrayList<String>();
        //Constructor for Muscle organs
        Muscles(){
            muscle_names.add("\u2605 gluteus");//<- random unicode implemented
            muscle_names.add("biceps");
            this.numOfOrgans = 2;
        }
        class Bicep extends Organ{
            int size = 12;
        }
        class ButtChecks extends Organ{
            int size = 30;
        }
        void info(){
            System.out.println("There are "+muscle_names.size()+" in Muscles class:");
            for(String x : muscle_names){
                System.out.println(x);
            }
            System.out.println("Energy needed to Move this organism's muscles: "+minimalRequired()+"N");
        }

        int minimalRequired() {
            Bicep leftBicep  = new Bicep();
            Bicep rightBicep  = new Bicep();
            ButtChecks booty  = new ButtChecks();
            this.totalSize  += rightBicep.size+leftBicep.size+booty.size;
            return this.totalSize;
        }
        public  Character organType(){
            return this.typeOrgan ='A';
        }
    }

    static class AnimalCell extends Cell{
        int centrioles = 2;
        int minimalRequired() {
            this.totalSize  += centrioles;
            return this.totalSize;
        }
    }

    static class PlantCell extends Cell{
        protected   int cell_wall = 20;
        protected   int chloroplast = 20;//why not protected ?  :)

        int minimalRequired() {
            this.totalSize  += cell_wall+chloroplast;
            return this.totalSize;
        }
    }
    //************* METHODS  CALL in Main BELOW ***********************

    // This method creates 2 different Cell class object
    // This methods are inherit from  abstract class Cell()
    // which contains a list of the same components both cells share
    public static void cellsSize(){
        System.out.println("Animal Cell: " +new PlantCell().minimalRequired()+" \u00B5m");//micro symbol
        System.out.println("Plant Cell: " +new AnimalCell().minimalRequired()+" \u00B5m");
    }

    // This method
    //parameterized method
    public static void printMuscles(MuscularSystem muscularSystem){
        muscularSystem.printMuscleNames();
    }

    public static void printJuansMuscles(){
        Human juan = new Human("juan", "human", 23);
        juan.humanMuscularSystem.muscles.info();
    }

    public static void printLimbs(){
        Human me = new Human("juan", "human", 23);
        System.out.println(me.name +" has "+me.limbs.returnLimbs() +" Number of limbs");
    }
    public static void howManyOrganSystems(){
        Human avargeHuman = new Human("Juan","Human", false);

        System.out.println("This is the number of Organs Systems "+ avargeHuman.name+" has :"+avargeHuman.howManyOrganSystems());
        for(String x : avargeHuman.organsystems){
            System.out.println("\t"+x +" system");
        }
    }
    //************* END of METHODS  CALL in Main  ***********************


    public static void main(String[] arg) {
        // native implementation described at beginning of program
        // also explained why did not implemented
        // printing heading for this method's output
        // Native n = new Native();
        // n.m()
        /*Starts callings*/
        ExamScopes printMessage = new ExamScopes();
        printMessage.displayGeneralMessage();
        String option = "10";                              //the option of the user
        Scanner scanner = new Scanner(System.in);       //text input scanner
        while (!option.equals("0")  ){
            // \t and \n are some of many literals used along with the unicode provided at butt cheeks organ
            System.out.println("\n\nEnter an option : \n" +
                    "\t0. exit\n" +
                    "\t1. print different cells \"sizes\"\n" +
                    "\t2. print 2 different organs from \033[3mMuscular System\033[0m \n"+  //<-italics unicode
                    "\t3. print organisim type\n" +
                    "\t4. print Juan's limbs\n"+
                    "\t5. print Juan's energy needed to move some muscles\n"+
                    "\t6. print organisim's number of organ systems\n");
            option = scanner.next();

            if(Integer.valueOf(option) <0) //throw implementation
                throw new ArithmeticException("option cannot be less to zero!");

            if(MAX_VALUE >   Integer.valueOf(option)  ){
                System.out.println("**********************");
                if(option.equals("1")){
                    //print all cell sizes
                    cellsSize();
                }else if(option.equals("2") ){
                    //print all muscles in musclar system
                    printMuscles(new MuscularSystem());
                }else if(option.equals("3") ){
                    System.out.println("\t\033[1mHuman info\033[0m"); //<- bold titles
                    new Human("randomName","Human");
                    System.out.println("\n\t\033[1mPlant info\033[0m");
                    new Plant( "Plant");
                }else if(option.equals("4") ){
                    printLimbs();
                }else if(option.equals("5") ){
                    printJuansMuscles();
                }else if(option.equals("6") ){
                    howManyOrganSystems();
                }else if(option.equals("0") ){
                    break;
                }else{
                    System.out.println("not an option. please try again");
                }
                System.out.println("**********************");
            }
        }
        /*Ends*/
    }
}

/*
 * Thank you professor! Keep the good work up!
 * - Juan Moraza
 * */
